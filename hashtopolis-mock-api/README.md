# Hashtopolis Mock API

This is a mock API that mimicks the Hashtopolis API.

It does not cover the the full functionality of the real API, but only what is needed for the Crackbox API to run.

The API is not reactive, so if you delete a task it will not actually delete anything, it will just respond with a:

```json
{
    "section": "task",
    "request": "deleteTask",
    "response": "OK"
}
```

You can find more details on the design decisions made in this project on the [Hashtopolis Mock API wiki page](https://gitlab.com/prexus/crackbox/-/wikis/hashtopolis-mock-api).

## Installation

```bash
$ yarn install
```

## Running the mock API

```bash
$ yarn start
```

After starting the application the endpoint is available on `http://localhost:3100/api/user.php`.

## Communicating with the API

The endpoint uses `POST` and expects JSON in the body, for instance:

```json
{
    "section": "test",
    "request": "access",
    "accessKey": "test"
}
```

For the full API documentation please refer to the user-api.pdf found in [Hashtopolis Github repo](https://github.com/hashtopolis/server/blob/master/doc/user-api/user-api.pdf)
