const mocks = {
    listAgents,
    get,
    setActive
};

function listAgents() {
    return {
        section: 'agent',
        request: 'listAgents',
        response: 'OK',
        agents: [
            {
                agentId: '1',
                name: 'Crackbox Node 2',
                devices: [
                    'Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz',
                    'NVIDIA Quadro 600'
                ]
            },
            {
                agentId: '2',
                name: 'Crackbox Node 3',
                devices: [
                    'Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz',
                    'NVIDIA Quadro 600'
                ]
            },
            {
                agentId: '3',
                name: 'Crackbox Node 4',
                devices: [
                    'Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz',
                    'NVIDIA Quadro 600'
                ]
            }
        ]
    };
}

function get() {
    return {
        section: 'agent',
        request: 'get',
        response: 'OK',
        name: 'Crackbox Node X',
        devices: [
            'Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz',
            'NVIDIA Quadro 600'
        ],
        owner: {
            userId: 1,
            username: 'htp'
        },
        isCpuOnly: false,
        isTrusted: true,
        isActive: true,
        token: '0lBfAp7YQh',
        extraParameters: '--force',
        errorFlag: 2,
        lastActivity: {
            action: 'getTask',
            time: 1531316240,
            ip: '127.0.0.1'
        }
    };
}

function setActive() {
    return {
        section: 'agent',
        request: 'setActive',
        response: 'OK'
    };
}

export const agentMocks = mocks;
