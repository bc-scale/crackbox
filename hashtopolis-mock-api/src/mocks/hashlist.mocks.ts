const mocks = {
    createHashlist,
    getHashlist,
    deleteHashlist,
    getCracked
};

function createHashlist() {
    return {
        section: 'hashlist',
        request: 'createHashlist',
        response: 'OK',
        hashlistId: 101
    };
}

function getHashlist() {
    return {
        section: 'hashlist',
        request: 'getHashlist',
        response: 'OK',
        hashlistId: 1,
        hashtypeId: 0,
        name: 'Hashcat Example',
        format: 0,
        hashCount: 6494,
        cracked: 3382,
        accessGroupId: 1,
        isHexSalt: false,
        isSalted: false,
        isSecret: false,
        saltSeparator: ':',
        notes: 'This hashlist is from blahblah...',
        useBrain: false
    };
}

function deleteHashlist() {
    return {
        section: 'hashlist',
        request: 'deleteHashlist',
        response: 'OK'
    };
}

function getCracked() {
    return {
        section: 'hashlist',
        request: 'getCracked',
        response: 'OK',
        cracked: [
            {
                hash: '098f6bcd4621d373cade4e832627b4f6',
                plain: 'test',
                crackpos: '634721'
            },
            {
                hash: '5f4dcc3b5aa765d61d8327deb882cf99',
                plain: 'password',
                crackpos: '608529'
            }
        ]
    };
}

export const hashlistMocks = mocks;
