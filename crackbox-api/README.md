# Crackbox API

The Crackbox API relies on an installation of Hashtopolis for fetching data that will be displayed in the UI. Itself does nothing but handling state and simplify some of the complexity offered by Hashtopolis.

For development, you can use the Hashtopolis Mock API available in this repo.

You can find more details on the design decisions made in this project on the [Crackbox API wiki page](https://gitlab.com/prexus/crackbox/-/wikis/crackbox-api).

## Installation

```bash
$ yarn install
```

## Running the app

Running Crackbox API requires Node.js v18+. Please note that `fetch` is still in the experimental stage and will show an _ExperimentalWarning_ the first time is called. This is expected.

```bash
$ yarn start
```

## Documentation

Documentation for the http endpoint is automatically generated using swagger and is available on `http://localhost:3000/docs`.

Websocket endpoint is not well documented apart from what you find in the code, but once connected you'll received JSON formatted updates, eg.:

```typescript
{
    agents: Agent[],
    tasks: Task[]
}
```

Interfaces for [Agent](./models/interfaces/agent.interface.ts) and [Task](./models/interfaces/task.interface.ts) are available in the models folder.

## Configuration
There are a few required environment variables that needs to be set for the API to work.

`HASHTOPOLIS_URL`<br>
_Required_

You must provide a URL to the Hashtopolis instance that the API needs to talk to. Tested with Hashtopolis v0.12.0.

`HASHTOPOLIS_API_KEY`<br>
_Required_

You must provide a valid API key from the running Hashtopolis instance.

`CRACKBOX_PORT`<br>
_Optional, default: 3000_

When deployed to the Crackbox the UI and API runs on port 80 to make it easier accessible for the users.


## Docker bundle
Gitlab will build a docker container of Crackbox API containing a static hosted version Crackbox UI. The container can be downloaded from `registry.gitlab.com/prexus/crackbox` and is available with both a `:latest` and a versioned tag.

Remember to set the environment variable `HASHTOPOLIS_URL` to point to your Hashtopolis installation and `HASHTOPOLIS_API_KEY` to contain a valid API key. Otherwise the container won't start. If you don't have a Hashtopolis installation available, you can start up the Hashtopolis Mock API instead and point it to that.

The container exposes port `3000` to serve the UI and API.

This is how you can start the container:

```bash
$ docker run -e HASHTOPOLIS_URL=http://my-hashtopolis-server.com -e HASHTOPOLIS_API_KEY=my-secret-api-key -p 3000:3000 registry.gitlab.com/prexus/crackbox:latest
```

Now open a browser on `http://localhost:3000` and see if everything works!
