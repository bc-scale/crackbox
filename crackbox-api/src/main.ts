import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { WsAdapter } from '@nestjs/platform-ws';

import { AppModule } from './app.module';

const logger = new Logger('Init');

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const port = process.env.CRACKBOX_PORT || 3000;

    app.useGlobalPipes(new ValidationPipe({
        transform: true,
        whitelist: true,
        forbidNonWhitelisted: true,
        forbidUnknownValues: true
    }));

    app.setGlobalPrefix('api');
    app.useWebSocketAdapter(new WsAdapter(app));

    const config = new DocumentBuilder()
        .setTitle('Crackbox API')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('docs', app, document);

    await app.listen(port);

    logger.log(`Crackbox available on port ${port}`);
}

function verifySettings() {
    if (!process.env.HASHTOPOLIS_URL) {
        logger.error('You must provide HASHTOPOLIS_URL to a Hashtopolis server as an environment variable');

        process.exit(1);
    }

    if (!process.env.HASHTOPOLIS_API_KEY) {
        logger.error('You must provide HASHTOPOLIS_API_KEY with a valid Hashtopolis API key');

        process.exit(1);
    }
}

verifySettings();
bootstrap();
