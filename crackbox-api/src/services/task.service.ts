import { Injectable } from '@nestjs/common';

import { Task } from '@cbox-api/interfaces/task.interface';

import { TaskStatus } from '@cbox-api/enums/task-status.enum';

import { GetTaskResponse } from '@cbox-api/dtos/get-task.dto';
import { ListTasksTaskEntry } from '@cbox-api/dtos/list-tasks.dto';
import { GetHashlistResponse } from '@cbox-api/dtos/get-hashlist.dto';

import { TaskRecipe } from '@cbox-api/classes/task-recipe';
import { TaskPatch } from '@cbox-api/classes/task-patch';

import { StateService } from './state.service';

import { HashTypeService } from './hash-type.service';

import { AgentApiService } from './api/agent-api.service';
import { HashlistApiService } from './api/hashlist-api.service';
import { TaskApiService } from './api/task-api.service';
import { ChunkService } from './chunk.service';

@Injectable()
export class TaskService {
    constructor(
        private agentApiService: AgentApiService,
        private hashlistApiService: HashlistApiService,
        private taskApiService: TaskApiService,
        private stateService: StateService,
        private chunkService: ChunkService,
        private hashTypeService: HashTypeService
    ) {}

    async getTasks() {
        const taskResponse = await this.taskApiService.listTasks();
        const taskPromises = taskResponse.tasks.map((task) => this.transformTask(task));
        const tasks = await Promise.all(taskPromises);

        return tasks;
    }

    async transformTask(listTaskResponse: ListTasksTaskEntry) {
        const details = await this.taskApiService.getTask(listTaskResponse.taskId);
        const hashlist = await this.hashlistApiService.getHashlist(listTaskResponse.hashlistId);

        const task: Task = {
            id: listTaskResponse.taskId,
            name: listTaskResponse.name,
            speed: details.speed,
            agents: details.agents.length,
            hashes: hashlist.hashCount,
            cracked: hashlist.cracked,
            status: await this.getTaskStatus(hashlist, details),
            progress: await this.chunkService.getChunkProgress(details),
            hashType: this.hashTypeService.getHashName(hashlist.hashtypeId),
            percentage: this.getTaskCompletionPercentage(hashlist, details)
        };

        return task;
    }

    async getTaskStatus(hashlist: GetHashlistResponse, details: GetTaskResponse) {
        if (details.agents.length > 0) {
            const failingAgents = await this.hasFailedAgents(details);

            return failingAgents ? TaskStatus.FAILED : TaskStatus.RUNNING;
        }

        if (details.priority === 0 && hashlist.cracked === hashlist.hashCount) {
            return TaskStatus.COMPLETED;
        }

        if (details.priority === 0) {
            return TaskStatus.CANCELLED;
        }

        return TaskStatus.SCHEDULED;
    }

    async hasFailedAgents(details: GetTaskResponse) {
        const agents = details.agents;

        for (const agent of agents) {
            const agentDetails = await this.agentApiService.getAgent(agent.agentId);

            if (!agentDetails.isActive) {
                return true;
            }
        }

        return false;
    }

    getTaskCompletionPercentage(hashlist: GetHashlistResponse, details: GetTaskResponse) {
        if (hashlist.cracked === hashlist.hashCount) {
            return 100;
        }

        if (details.keyspace > 0) {
            const percentage = (details.searched / details.keyspace) * 100;
            const pctRounded = Math.round((percentage + Number.EPSILON) * 100) / 100;

            return pctRounded;
        }

        return 0;
    }

    async createTask(recipe: TaskRecipe) {
        const { hashlistId } = await this.hashlistApiService.createHashlist(recipe.name, recipe.data, recipe.hashTypeId);
        const { taskId } = await this.taskApiService.createTask(recipe.name, hashlistId);

        // Notify clients
        const tasks = await this.getTasks();

        this.stateService.updateTasks(tasks);

        return { taskId };
    }

    async deleteTask(taskId: number) {
        const task = await this.taskApiService.getTask(taskId);

        const deleteTaskResponse = await this.taskApiService.deleteTask(task.taskId);
        const success = (deleteTaskResponse.response === 'OK');

        try {
            // This might fail due to an error in hashtopolis. The error was
            // reported here: https://github.com/hashtopolis/server/issues/747
            await this.hashlistApiService.deleteHashlist(task.hashlistId);
        } catch (error) {
            // We'll not do anything for now
        }

        return { success };
    }

    async updateTask(update: TaskPatch) {
        const taskId = update.taskId;
        const priority = update.active ? 1 : 0;

        const setTaskPriorityResponse = await this.taskApiService.setTaskPriority(taskId, priority);

        if (setTaskPriorityResponse.response !== 'OK') {
            throw new Error('Unable to set task priority');
        }
    }

    async getCrackedPasswords(taskId: number) {
        const { hashlistId } = await this.taskApiService.getTask(taskId);
        const { cracked } = await this.hashlistApiService.getCracked(hashlistId);

        const passwords = cracked.map((password) => `${password.hash}:${password.plain}`);

        return passwords;
    }
}
