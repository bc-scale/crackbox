import { Module } from '@nestjs/common';

import { ApiService } from './api/api.service';
import { AgentApiService } from './api/agent-api.service';
import { HashlistApiService } from './api/hashlist-api.service';
import { TaskApiService } from './api/task-api.service';

import { AgentService } from './agent.service';
import { ChunkService } from './chunk.service';
import { CronService } from './cron.services';
import { DataService } from './data.service';
import { EventService } from './event.service';
import { HashTypeService } from './hash-type.service';
import { StateService } from './state.service';
import { TaskService } from './task.service';

const allServices = [
    // API Services
    ApiService,
    HashlistApiService,
    TaskApiService,
    AgentApiService,

    // Utility services
    AgentService,
    ChunkService,
    CronService,
    DataService,
    EventService,
    HashTypeService,
    StateService,
    TaskService
];

@Module({
    providers: allServices,
    exports: allServices
})
export class ServiceModule {}
