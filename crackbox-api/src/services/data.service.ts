import { Injectable } from '@nestjs/common';

import { HashType } from '@cbox-api/interfaces/hash-type.interface';

import hashcatHashTypes from '@cbox-api/data/hash-types.json';
import hashcatFiles from '@cbox-api/data/files.json';

@Injectable()
export class DataService {
    private hashTypes = hashcatHashTypes.map((hashType) => ({
        id: hashType.hashTypeId,
        name: hashType.description
    }));

    // Unfortunately hashtopolis does not provide an endpoint for this
    // so instead I'm using a fixed json matching the data it inserts
    // in the database upon install. This could get out of sync if it
    // is not upgraded together with the hashcat/hashtopolis versions.
    getHashTypes(): HashType[] {
        return this.hashTypes;
    }

    // Static files orcestrated by the deployment project
    getFiles() {
        return hashcatFiles;
    }
}
