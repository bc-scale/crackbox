import { Injectable } from '@nestjs/common';

import { GetAgentRequest, GetAgentResponse } from '@cbox-api/dtos/get-agent.dto';
import { ListAgentsRequest, ListAgentsResponse } from '@cbox-api/dtos/list-agents.dto';
import { SetActiveRequest, SetActiveResponse } from '@cbox-api/dtos/set-active.dto';

import { ApiService } from './api.service';

@Injectable()
export class AgentApiService extends ApiService {
    listAgents() {
        const query = {
            section: 'agent',
            request: 'listAgents'
        };

        return this.send<ListAgentsRequest, ListAgentsResponse>(query);
    }

    getAgent(id: number) {
        const query = {
            section: 'agent',
            request: 'get',
            agentId: id.toString()

        };

        return this.send<GetAgentRequest, GetAgentResponse>(query);
    }

    setActive(agentId: number, active: boolean) {
        const query = {
            section: 'agent',
            request: 'setActive',
            agentId: agentId,
            active: active
        };

        return this.send<SetActiveRequest, SetActiveResponse>(query);
    }
}
