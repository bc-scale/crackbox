import { Injectable } from '@nestjs/common';

import { DataService } from './data.service';

@Injectable()
export class HashTypeService {
    constructor(private dataService: DataService) {}

    getHashTypes() {
        return this.dataService.getHashTypes();
    }

    getHashName(hashTypeId: number) {
        const hashTypes = this.dataService.getHashTypes();
        const hashType = hashTypes.find((type) => type.id === hashTypeId);

        return hashType.name;
    }
}
