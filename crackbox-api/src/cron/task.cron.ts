import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { CronService } from '@cbox-api/services/cron.services';
import { TaskService } from '@cbox-api/services/task.service';

import { TaskStatus } from '@cbox-api/enums/task-status.enum';

@Injectable()
export class TaskCron {
    private logger = new Logger(TaskCron.name);

    constructor(
        private cronService: CronService,
        private taskService: TaskService
    ) { }

    @Cron('*/10 * * * * *')
    async taskMaintenance() {
        this.cronService.run('taskMaintenance', async () => {
            const tasks = await this.taskService.getTasks();
            const failedTasks = tasks.filter((task) => task.status === TaskStatus.FAILED);

            for (const task of failedTasks) {
                this.logger.log(`Task #${task.id} failed, cancelling task`);

                const update = { taskId: task.id, active: false };

                await this.taskService.updateTask(update);
            }
        });
    }
}
