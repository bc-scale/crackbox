export enum TaskStatus {
    CANCELLED = 'cancelled',
    FAILED = 'failed',
    RUNNING = 'running',
    SCHEDULED = 'scheduled',
    COMPLETED = 'completed'
}
