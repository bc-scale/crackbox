export enum ChunkStatus {
    PENDING,
    RUNNING,
    COMPLETED,
    FAILED
}
