import { BlockStatus } from '@cbox-api/enums/block-status.enum';
import { ChunkStatus } from '@cbox-api/enums/chunk-status.enum';

export interface Chunk {
    chunkId?: number;
    agent?: string;
    cracked: number;
    progress: number;
    status: ChunkStatus;
    blocks: BlockStatus[];
}
