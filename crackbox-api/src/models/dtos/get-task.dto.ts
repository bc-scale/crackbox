import { ApiRequest, ApiResponse } from './api.dto';

export interface GetTaskRequest extends ApiRequest {
    taskId: number;
}

export interface GetTaskResponse extends ApiResponse {
    taskId: number;
    name: string;
    attack: string;
    chunksize: number;
    color: string;
    benchmarkType: string;
    statusTimer: number;
    priority: number;
    isCpuOnly: boolean;
    isSmall: boolean;
    skipKeyspace: number;
    keyspace: number;
    dispatched: number;
    hashlistId: number;
    imageUrl: string;
    files: {
        fileId: number;
        filename: string;
        size: number;
    }[];
    speed: number;
    searched: number;
    chunkIds: number[];
    agents: {
        agentId: number;
        benchmark: string;
        speed: number;
    }[];
    isComplete: boolean;
    workPossible: boolean;
    usePreprocessor: boolean;
    preprocessorId: number;
    preprocessorCommand: string;
}
