import { ApiRequest, ApiResponse } from './api.dto';

export interface GetHashlistRequest extends ApiRequest {
    hashlistId: number;
}

export interface GetHashlistResponse extends ApiResponse {
    hashlistId: number;
    hashtypeId: number;
    name: string;
    format: number;
    hashCount: number;
    cracked: number;
    accessGroupId: number;
    isHexSalt: boolean;
    isSalted: boolean;
    isSecret: boolean;
    saltSeparator: string;
    notes: string;
    useBrain: boolean;
}
