import { ApiRequest, ApiResponse } from './api.dto';

export interface GetChunkRequest extends ApiRequest {
    chunkId: number;
}

export interface GetChunkResponse extends ApiResponse {
    chunkId: number;
    start: number;
    length: number;
    checkpoint: number;
    progress: number;
    taskId: number;
    agentId: number;
    dispatchTime: number;
    lastActivity: number;
    state: number;
    cracked: number;
    speed: number;
}
