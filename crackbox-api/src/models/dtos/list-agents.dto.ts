import { ApiRequest, ApiResponse } from './api.dto';

export interface ListAgentsRequest extends ApiRequest { }

export interface ListAgentsResponse extends ApiResponse {
    section: string;
    request: string;
    response: string;
    agents: [
        {
            agentId: string;
            name: string;
            devices: string[]
        }
    ]
}
