import { ApiRequest, ApiResponse } from './api.dto';

export interface DeleteHashlistRequest extends ApiRequest {
    hashlistId: number;
}

export interface DeleteHashlistResponse extends ApiResponse { }
