import { ApiRequest, ApiResponse } from './api.dto';

export interface GetAgentRequest extends ApiRequest {
    agentId: string;
}

export interface GetAgentResponse extends ApiResponse {
    section: string;
    request: string;
    response: string;
    name: string;
    devices: string[],
    owner: {
        userId: number;
        username: string;
    },
    isCpuOnly: boolean;
    isTrusted: boolean;
    isActive: boolean;
    token: string;
    extraParameters: string;
    errorFlag: number;
    lastActivity: {
        action: string;
        time: number;
        ip: string;
    }
}
