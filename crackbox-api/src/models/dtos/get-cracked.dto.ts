import { ApiRequest, ApiResponse } from './api.dto';

export interface GetCrackedRequest extends ApiRequest {
    hashlistId: number
}

export interface GetCrackedResponse extends ApiResponse {
    cracked: {
        hash: string;
        plain: string;
        crackpos: string;
    }[]
}
