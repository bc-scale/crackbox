import { ApiRequest, ApiResponse } from './api.dto';

export interface SetActiveRequest extends ApiRequest {
    agentId: number;
    active: boolean;
}

export interface SetActiveResponse extends ApiResponse { }
