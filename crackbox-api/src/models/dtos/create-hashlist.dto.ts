import { ApiRequest, ApiResponse } from './api.dto';

export interface CreateHashlistRequest extends ApiRequest {
    name: string;
    isSalted: boolean;
    isSecret: boolean;
    isHexSalt: boolean;
    separator: string;
    format: number;
    hashtypeId: number;
    accessGroupId: number;
    data: string;
    useBrain: boolean;
    brainFeatures: number;
}

export interface CreateHashlistResponse extends ApiResponse {
    hashlistId: number;
}
