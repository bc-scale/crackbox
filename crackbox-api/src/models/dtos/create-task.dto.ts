import { ApiRequest, ApiResponse } from './api.dto';

export interface CreateTaskRequest extends ApiRequest {
    name: string;
    hashlistId: number;
    attackCmd: string;
    chunksize: number;
    statusTimer: number;
    benchmarkType: string;
    color: string;
    isCpuOnly: boolean;
    isSmall: boolean;
    skip: number;
    crackerVersionId: number;
    files: number[];
    priority: number;
    preprocessorId: number;
    preprocessorCommand: string;
}

export interface CreateTaskResponse extends ApiResponse {
    taskId: number;
}
