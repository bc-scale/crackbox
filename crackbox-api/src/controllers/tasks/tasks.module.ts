import { Module } from '@nestjs/common';

import { ServiceModule } from '@cbox-api/services/service.module';

import { TasksController } from './tasks.controller';

@Module({
    imports: [ServiceModule],
    controllers: [TasksController]
})
export class TasksModule { }
