import { registerDecorator } from 'class-validator';

import hashcatHashTypes from '@cbox-api/data/hash-types.json';

export function IsHashTypeId() {
    return (object: Object, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            validator: {
                validate(hashTypeId: number) {
                    return !!hashcatHashTypes.find((ht) => ht.hashTypeId === hashTypeId);
                },
                defaultMessage() {
                    return 'the provide hashTypeId is not found in the database';
                }
            }
        });
    };
}
