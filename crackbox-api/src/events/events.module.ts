import { Module } from '@nestjs/common';

import { ServiceModule } from '@cbox-api/services/service.module';

import { EventsGateway } from './events.gateway';

@Module({
    imports: [ServiceModule],
    providers: [EventsGateway]
})
export class EventsModule {}
