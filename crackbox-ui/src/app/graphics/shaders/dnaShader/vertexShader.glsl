varying vec2 vUv;
varying vec3 vPosition;
varying float vRandomColor;
varying vec3 vColor;

attribute float size;
attribute vec3 color;

void main() {
  vUv = uv;
  vColor = color;

  vec4 mvPosition = modelViewMatrix * vec4( position, 1. );

  gl_PointSize = (25. * size + 45.) * ( 1. / - mvPosition.z );
  gl_Position = projectionMatrix * mvPosition;
}
