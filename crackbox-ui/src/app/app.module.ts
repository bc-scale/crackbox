import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DnaModule } from './modules/dna/dna.module';
import { StatusModule } from './modules/status/status.module';
import { TasksModule } from './modules/tasks/tasks.module';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        DnaModule,
        StatusModule,
        TasksModule
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
