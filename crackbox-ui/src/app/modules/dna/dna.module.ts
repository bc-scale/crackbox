import { NgModule } from '@angular/core';

import { DnaComponent } from './dna.component';

@NgModule({
    declarations: [
        DnaComponent
    ],
    exports: [
        DnaComponent
    ]
})
export class DnaModule { }
