import { Component, DoCheck, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { IterableDiffer, IterableDiffers } from '@angular/core';

import * as THREE from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

import { aberrationShader } from '@cbox/shaders/aberrationShader/aberrationShader';

import { Dna } from '@cbox/geometries/dna';

@Component({
    template: '',
    selector: 'cbox-dna',
    styleUrls: ['./dna.component.scss']
})
export class DnaComponent implements OnInit, OnDestroy, DoCheck {
    @Input() basePairProgressRows: number = 30;
    @Input() basePairWidth: number = 100;
    @Input() progress: number[] = [];

    private clock = new THREE.Clock();
    private scene = new THREE.Scene();
    private camera = new THREE.PerspectiveCamera();
    private renderer = new THREE.WebGLRenderer();

    private composer = new EffectComposer(this.renderer);

    private container: HTMLElement;
    private iterableDiffer: IterableDiffer<number>;

    private dna: Dna;

    private resizeObserver: ResizeObserver;

    constructor(
        hostElement: ElementRef,
        iterableDiffers: IterableDiffers
    ) {
        this.container = hostElement.nativeElement;
        this.iterableDiffer = iterableDiffers.find([]).create<number>(null);
    }

    ngOnInit() {
        this.setupRenderer();
        this.setupCamera();
        // this.setupControls();
        this.setupScene();
        this.setupPostProcessing();
        this.setupResizeObserver();

        this.render();
    }

    ngOnDestroy() {
        this.disconnectResizeObserver();
    }

    ngDoCheck() {
        this.checkProgressUpdates();
    }

    setupRenderer() {
        const width = this.container.offsetWidth;
        const height = this.container.offsetHeight;

        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(width, height);
        this.renderer.setClearColor(0x1f1f26, 1);

        this.container.appendChild(this.renderer.domElement);
    }

    setupCamera() {
        this.camera.position.set(0, 0, 20);
    }

    setupControls() {
        const controls = new OrbitControls(this.camera, this.renderer.domElement);

        controls.enabled = true;
    }

    setupScene() {
        const basePairOptions = {
            basePairWidth: this.basePairWidth,
            basePairProgressRows: this.basePairProgressRows,
            basePairCount: 350,
            basePairHorizontalSpacing: 0.15,
            basePairVerticalSpacing: 0.15,
            // Percentage of the circumference to twist
            // per base pair for each row (0,005 = half a percent)
            basePairOffset: 0.005
        };

        this.dna = new Dna(this.progress, basePairOptions);

        const model = this.dna.getModel();

        // Places model in scene a little tilted backwards and
        // to the side to make it look a little less stiff
        model.rotation.set(-0.2, 0, -0.1);

        this.scene.add(model);
    }

    setupResizeObserver() {
        this.resizeObserver = new ResizeObserver(() => this.resize());
        this.resizeObserver.observe(this.container);

        this.resize();
    }

    disconnectResizeObserver() {
        this.resizeObserver.disconnect();
    }

    setupPostProcessing() {
        const renderScene = new RenderPass(this.scene, this.camera);

        const width = this.container.offsetWidth;
        const height = this.container.offsetHeight;
        const resolution = new THREE.Vector2(width, height);

        const bloomThreshold = 0.3;
        const bloomStrength = 0.8;
        const bloomRadius = 0.3;

        const bloomPass = new UnrealBloomPass(resolution, bloomStrength, bloomRadius, bloomThreshold);
        const aberrationPass = new ShaderPass(aberrationShader);

        this.composer.addPass(renderScene);
        this.composer.addPass(bloomPass);
        this.composer.addPass(aberrationPass);
    }

    resize() {
        window.requestAnimationFrame(() => {
            const width = this.container.offsetWidth;
            const height = this.container.offsetHeight;

            this.renderer.setSize(width, height);
            this.composer.setSize(width, height);

            this.camera.aspect = width / height;
            this.camera.updateProjectionMatrix();
        });
    }

    render() {
        this.spin();

        this.composer.render();

        requestAnimationFrame(() => this.render());
    }

    spin() {
        // Rotation speed is degrees per second
        const speed = -6;
        const model = this.dna.getModel();
        const delta = this.clock.getDelta();
        const angle = (speed * delta * Math.PI) / 180;

        model.rotateY(angle);
    }

    checkProgressUpdates() {
        const progressUpdated = this.iterableDiffer.diff(this.progress);

        if (progressUpdated) {
            this.dna.rebuildActiveBasePairs(this.progress);
        }
    }
}
