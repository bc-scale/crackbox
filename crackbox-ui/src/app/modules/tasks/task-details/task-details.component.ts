import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Task } from '@cbox-api/interfaces/task.interface';
import { TaskService } from '@cbox/services/task.service';

@Component({
    selector: 'cbox-task-details',
    templateUrl: './task-details.component.html',
    styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent {
    @Input() task: Task;

    @Output() taskDeleted = new EventEmitter<Task>();

    confirmDeletion = false;

    constructor(private taskService: TaskService) { }

    deleteTask() {
        this.confirmDeletion = true;
    }

    async confirmDeleteTask(confirmed: boolean) {
        if (!confirmed) {
            this.confirmDeletion = false;

            return;
        }

        const response = await this.taskService.deleteTask(this.task);

        if (response.success) {
            this.taskDeleted.emit(this.task);
        }
    }

    downloadPasswords() {
        window.location.href = `/api/tasks/${this.task.id}/download`;
    }
}
