import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { TaskService } from '@cbox/services/task.service';

import { HashType } from '@cbox-api/interfaces/hash-type.interface';

@Component({
    selector: 'cbox-create-task',
    templateUrl: './create-task.component.html',
    styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {
    name = '';
    hashes = '';
    hashTypeId = 0;

    hashTypes: HashType[] = [];

    constructor(private taskService: TaskService) { }

    @Output() closeCreateTask = new EventEmitter<void>();
    @Output() taskCreated = new EventEmitter<void>();

    async ngOnInit() {
        this.hashTypes = await this.taskService.getHashTypes();
    }

    async createTask() {
        await this.taskService.createTask(this.name, this.hashes, this.hashTypeId);

        this.taskCreated.emit();
    }

    close() {
        this.closeCreateTask.emit();
    }
}
