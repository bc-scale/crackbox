import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { EventService } from '@cbox/services/event.service';

import { AgentComponent } from './agent/agent.component';
import { ConnectionComponent } from './connection/connection.component';

import { StatusComponent } from './status.component';

@NgModule({
    declarations: [
        AgentComponent,
        ConnectionComponent,
        StatusComponent
    ],
    providers: [
        EventService
    ],
    imports: [
        BrowserModule
    ],
    exports: [
        StatusComponent
    ]
})
export class StatusModule { }
