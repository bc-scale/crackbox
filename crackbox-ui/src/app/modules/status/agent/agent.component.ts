import { Component, Input } from '@angular/core';

import { Agent } from '@cbox-api/interfaces/agent.interface';

@Component({
    selector: 'cbox-agent',
    templateUrl: './agent.component.html',
    styleUrls: ['./agent.component.scss']
})
export class AgentComponent {
    @Input() agent!: Agent;
}
