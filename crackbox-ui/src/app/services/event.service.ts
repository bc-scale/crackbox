import { Injectable } from '@angular/core';

import { BehaviorSubject, retry, Subject } from 'rxjs';
import { webSocket } from 'rxjs/webSocket';

import { Agent } from '@cbox-api/interfaces/agent.interface';
import { Task } from '@cbox-api/interfaces/task.interface';

@Injectable()
export class EventService {
    public connected$ = new BehaviorSubject<boolean>(false);

    public agents$ = new Subject<Agent[]>();
    public tasks$ = new Subject<Task[]>();

    constructor() {
        const host = window.location.host;
        const protocol = window.location.protocol === 'https:' ? 'wws' : 'ws';

        const subject = webSocket<any>({
            url: `${protocol}://${host}/websocket`,
            openObserver: { next: () => this.setConnected(true) },
            closeObserver: { next: () => this.setConnected(false) }
        });

        subject.pipe(retry()).subscribe({
            next: (data) => this.onData(data),
            error: () => this.setConnected(false),
            complete: () => this.setConnected(false)
        });
    }

    private onData(data: any) {
        if (data.agents) {
            this.agents$.next(data.agents);
        }

        if (data.tasks) {
            this.tasks$.next(data.tasks);
        }
    }

    private setConnected(connected: boolean) {
        this.connected$.next(connected);
    }
}
