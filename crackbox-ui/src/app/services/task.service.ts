import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { firstValueFrom } from 'rxjs';

import { Task } from '@cbox-api/interfaces/task.interface';
import { HashType } from '@cbox-api/interfaces/hash-type.interface';

interface CreateTaskResponse {
    taskId: number;
}

interface DeleteTaskResponse {
    success: boolean
}

@Injectable()
export class TaskService {
    constructor(private httpClient: HttpClient) { }

    public getHashTypes() {
        const response$ = this.httpClient.get<HashType[]>('/api/hash-types');

        return firstValueFrom(response$);
    }

    public createTask(taskName: string, hashes: string, linkedHashTypeId: number) {
        const query = {
            name: taskName,
            data: btoa(hashes),
            hashTypeId: linkedHashTypeId
        };

        const response$ = this.httpClient.post<CreateTaskResponse>('/api/tasks', query);

        return firstValueFrom(response$);
    }

    public deleteTask(task: Task) {
        const response$ = this.httpClient.delete<DeleteTaskResponse>(`/api/tasks/${task.id}`);

        return firstValueFrom(response$);
    }

    public convertChunkProgress(chunkProgress: number[], rows: number, columns: number) {
        const totalPoints = rows * columns;

        const chunkPoints = this.getPointsPerChunk(chunkProgress, totalPoints);
        const progressPoints = this.getProgressPoints(chunkProgress, chunkPoints);

        return progressPoints;
    }

    getPointsPerChunk(chunkProgress: number[], totalPoints: number) {
        const totalChunks = chunkProgress.length;
        const pointsPerChunk = Math.floor(totalPoints / totalChunks);

        let unassignedPoints = totalPoints - (pointsPerChunk * totalChunks);

        const chunkPoints: number[] = [];

        for (let i = 0; i < totalChunks; i++) {
            let assignedPoints = pointsPerChunk;

            if (unassignedPoints > 0) {
                assignedPoints++;
                unassignedPoints--;
            }

            chunkPoints.push(assignedPoints);
        }

        return chunkPoints;
    }

    getProgressPoints(chunkProgress: number[], chunkPoints: number[]) {
        const totalChunks = chunkPoints.length;

        let pointsStart = 0;
        const progress = [];

        for (let i = 0; i < totalChunks; i++) {
            const assignedPoints = chunkPoints[i];
            const taskProgress = chunkProgress[i];
            const activePoints = (assignedPoints * taskProgress) / 100;

            for (let point = 0; point < activePoints; point++) {
                progress.push(pointsStart + point);
            }

            pointsStart += assignedPoints;
        }

        return progress;
    }
}
