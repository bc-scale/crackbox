import { NgModule } from '@angular/core';

import { HashRatePipe } from './hash-rate.pipe';

@NgModule({
    declarations: [HashRatePipe],
    exports: [HashRatePipe]
})
export class PipesModule { }
