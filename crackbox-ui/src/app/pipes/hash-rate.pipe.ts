import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'hashRate' })
export class HashRatePipe implements PipeTransform {
    transform(speed: number): any {
        if (speed === 0) return 'idle';

        const k = 1024;
        const sizes = ['H/s', 'KH/s', 'MH/s', 'GH/s', 'TH/s', 'PH/s', 'EH/s', 'ZH/s', 'YH/s'];

        const i = Math.floor(Math.log(speed) / Math.log(k));

        return `${parseFloat((speed / k ** i).toFixed(2))} ${sizes[i]}`;
    }
}
