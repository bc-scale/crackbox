/* CREATE USER AND DATABASE FOR HASHTOPOLIS */
CREATE USER 'hashtopolis'@'localhost' IDENTIFIED BY 'hashtopolis';
CREATE DATABASE hashtopolis;
GRANT ALL PRIVILEGES ON hashtopolis.* TO 'hashtopolis'@'localhost';
FLUSH PRIVILEGES;

