/* SELECT DATABSE */
USE hashtopolis;

/* INSERTS ADMINISTRATOR USER WITH DEFAULT admin/admin CREDENTIALS */
INSERT INTO AccessGroup VALUES (1, 'Default Group');
INSERT INTO User VALUES (1, 'admin', 'admin@admin.com', '$2y$12$rniShXTrBap6wbKUUfzJvu5knNhV9J/3dedCVUnVgD1b4UwfgEXH6', 'IGoADpeR5XjJ7bl0VTzi', 1, 1, 0, UNIX_TIMESTAMP(), 3600, 1, 0, '', '', '', '');
INSERT INTO AccessGroupUser VALUES (1, 1, 1);

/* DEFAULT VOUCHER FOR ALL CLIENTS */
INSERT INTO RegVoucher ( voucher , time ) VALUES ( 'hashtopolis' , UNIX_TIMESTAMP() );

/* DEFAULT API KEY FOR ALL CLIENTS VALID FOR 10 YEARS */
INSERT INTO ApiKey ( startValid , endValid , accessKey , accessCount , userId , apiGroupId ) VALUES ( UNIX_TIMESTAMP() , UNIX_TIMESTAMP() + (31536000 * 10) , 'crackbox' , 0 , 1 , 1 );

/* ENSURES VOUCHER IS ONT DELETED */
UPDATE Config SET value = 1 WHERE item = 'voucherDeletion';

/* CREATE CUSTOM INJECTED FILES IN DATABASE */
INSERT INTO File VALUES (1, 'rockyou.txt',               139921497, 0, 0, 1);
INSERT INTO File VALUES (2, 'OneRuleToRuleThemAll.rule',    402732, 0, 1, 1);
INSERT INTO File VALUES (3, 'best64.rule',                     933, 0, 1, 1);
