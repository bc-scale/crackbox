#!/bin/sh

# Create build folder
mkdir -v build

# Move to the build folder
cd build

# Get / set configuration (the default one worked for me)
cmake ..

# Compile the code
make -j `nproc`

# Install everything
sudo make install

# Create required symlink
ln -s /usr/local/etc/OpenCL /etc/OpenCL
